import { Navigation } from 'react-native-navigation';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from './src/screens';
import { useServerStore } from './src/stores/server.stores';

const App: ScreenFC = () => {
  const server = useServerStore();
  console.log(server.getServer);
  if (!server.getServer.sid) {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: screens.AUTHENTICATION,
              },
            },
          ],
        },
      },
    });
    return null;
  }

  if (server.getServer?.sid) {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: screens.HOME,
              },
            },
          ],
        },
      },
    });
    return null;
  }

  return null;
};

App.screenName = screens.INIT;
App.options = () => ({
  statusBar: {
    visible: false,
    drawBehind: true,
  },
  topBar: {
    visible: false,
    drawBehind: true,
  },
});

export default App;
