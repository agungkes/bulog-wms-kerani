import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import converDpToPixel from '../../helpers/convertDpToPixel';

const RADIO_BUTTON_SIZE = 20;
const styles = StyleSheet.create({
  radioButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: heightPercentageToDP(3),
  },
  radioButton: {
    height: widthPercentageToDP(converDpToPixel(RADIO_BUTTON_SIZE)),
    width: widthPercentageToDP(converDpToPixel(RADIO_BUTTON_SIZE)),
    backgroundColor: '#F8F8F8',
    borderRadius: widthPercentageToDP(converDpToPixel(RADIO_BUTTON_SIZE)),
    borderWidth: 1,
    borderColor: '#E6E6E6',
    alignItems: 'center',
    justifyContent: 'center',
  },
  radioButtonSelected: {
    borderColor: '#e97e11',
  },
  radioButtonIcon: {
    height: widthPercentageToDP(converDpToPixel(14)),
    width: widthPercentageToDP(converDpToPixel(14)),
    borderRadius: 7,
    backgroundColor: '#e97e11',
  },
  radioButtonText: {
    fontSize: widthPercentageToDP(converDpToPixel(16)),
    marginLeft: widthPercentageToDP(converDpToPixel(7)),
  },
});

export default styles;
