import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: heightPercentageToDP('85%'),
  },

  emptyLogo: {
    width: widthPercentageToDP(100),
    height: heightPercentageToDP(10),
    tintColor: 'gray',
  },
  emptyText: {
    fontSize: heightPercentageToDP(2),
    marginTop: heightPercentageToDP(2),
    color: 'grey',
  },
});

export default styles;
