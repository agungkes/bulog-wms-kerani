import { StyleSheet, Dimensions } from 'react-native';

const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  backgroundImage: {
    width: screen.width,
    height: screen.height,
    ...StyleSheet.absoluteFillObject,
  },
});
export default styles;
