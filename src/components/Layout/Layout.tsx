import React from 'react';
import { Image } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import styles from './Layout.styles';

const Layout: React.FC = ({ children }) => {
  return (
    <SafeAreaView>
      <Image
        source={require('../../assets/images/background.png')}
        style={styles.backgroundImage}
      />
      {children}
    </SafeAreaView>
  );
};

export default Layout;
