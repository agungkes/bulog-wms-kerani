import { Navigation } from 'react-native-navigation';

const screens = {
  INIT: 'INIT',
  AUTHENTICATION: 'Authentication',
  HOME: 'Home',

  LOADING: 'Loading',
  NOTIFICATION: 'NOTIFICATION',

  SCAN_QR: 'ScanQR',
  SURAT_JALAN_LIST: 'SuratJalanList',
  SURAT_JALAN_DETAIL: 'SuratJalanDetail',
  INCOMING_LIST: 'IncomingList',
  INCOMING_DETAIL: 'IncomingDetail',
  PUTAWAY_LIST: 'PutawayList',
  PUTAWAY_DETAIL: 'PutawayDetail',
  PICKING_LIST: 'PickingList',
  PICKING_DETAIL: 'PickingDetail',

  LOGOUT_CONFIRMATION: 'LogoutConfirmation',
};

export default screens;

export const appNavigation = () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: screens.HOME,
            },
          },
        ],
      },
    },
  });
};

export const authNavigation = () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: screens.AUTHENTICATION,
            },
          },
        ],
      },
    },
  });
};
