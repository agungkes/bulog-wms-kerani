const state: Record<string, string> = {
  draft: 'Draft',
  approved: 'Approved',
  done: 'Done',
  cancel: 'Cancel',
};

export default state;
