import { storeServer } from 'stores/server.stores';

type Params = {
  domain?: (string | number | (string | number)[])[][];
  offset?: number | false;
  limit?: number | false;
  sort?: string | false;
  fields?: string[];
  context?: any;
  method?: string;
  args?: any[];
  kwargs?: any;
};

const odooClient = function () {
  return {
    _cookie: '',
    _url: function (method: string) {
      return `${storeServer.getServer.protocol}://${storeServer.getServer.domain}/web/dataset/${method}`;
    },
    _params: function (model: string, params?: Params) {
      return JSON.stringify({
        jsonrpc: '2.0',
        id: new Date().getUTCMilliseconds(),
        method: 'call',
        params: {
          model,
          ...params,
        },
      });
    },
    _request: function (method: string, model: string, params?: Params) {
      return fetch(this._url(method), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Cookie: `session_id=${storeServer.getServer.sid}`,
        },
        credentials: 'omit',
        body: this._params(model, params),
      }).then(res => res.json());
    },
    connect: async function ({ db, username, password, protocol, host }: any) {
      const params = {
        db,
        login: username,
        password: password,
      };
      const json = JSON.stringify({ params: params });
      return fetch(`${protocol}://${host}/web/session/authenticate`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: json,
        credentials: 'omit',
      })
        .then(res => res.json())
        .then(res => {
          storeServer.setServer = {
            sid: res.result.session_id,
            cookie: res.result.session_id,
            username: username,
            password: password,
          };
          this._cookie = res.result.session_id;

          return {
            uid: res.result.uid,
            session_id: res.result.session_id,
            context: res.result.user_context,
            username: res.result.username,
          };
        });
    },
    search_read: async function <T = any>(
      model: string,
      params?: Params,
    ): Promise<T> {
      return this._request('search_read', model, {
        domain: [],
        ...params,
      }).then(res => res?.result?.records || []);
    },
    get: function <T = any>(model: string, params?: Params): Promise<T> {
      return this._request(`call_kw/${model}/read`, model, {
        ...params,
        method: 'read',
        kwargs: {},
      }).then(res => res?.result || []);
    },
    rpc_call: function (method: string, model: string, params?: Params) {
      return fetch(this._url(method), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Cookie: `session_id=${storeServer.getServer.sid}`,
        },
        credentials: 'omit',
        body: this._params(model, params),
      }).then(res => res.json());
    },
  };
};

const odoo = odooClient();
export default odoo;
