import { showOverlay, useNavigation } from 'react-native-navigation-hooks/dist';
import screens from 'screens';

const useLoadingScreen = () => {
  const { dismissOverlay } = useNavigation(screens.LOADING);
  const show = async () => {
    await showOverlay({
      component: { id: screens.LOADING, name: screens.LOADING },
    });
  };
  const dismiss = () => dismissOverlay();

  return [show, dismiss];
};

export default useLoadingScreen;
