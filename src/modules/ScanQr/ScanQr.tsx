import React from 'react';
import { Pressable, View } from 'react-native';
import { Icon, useTheme } from 'react-native-elements';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './ScanQr.styles';

import { BarCodeReadEvent, RNCamera } from 'react-native-camera';
import { BarcodeMask } from '@nartc/react-native-barcode-mask';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import useToggle from '@hooks/useToggle';

const ScanQr: ScreenFC = () => {
  const { theme } = useTheme();
  const [showFlashlight, setShowFlashlight] = useToggle();

  const handleBarcodeRead = (event: BarCodeReadEvent) => {
    console.log(event);
  };
  return (
    <View style={styles.container}>
      <RNCamera
        autoFocus={RNCamera.Constants.AutoFocus.on}
        style={styles.preview}
        type={RNCamera.Constants.Type.back}
        flashMode={
          showFlashlight
            ? RNCamera.Constants.FlashMode.torch
            : RNCamera.Constants.FlashMode.off
        }
        onBarCodeRead={handleBarcodeRead}>
        <BarcodeMask
          width={300}
          height={200}
          animatedLineThickness={1}
          backgroundColor={theme.colors?.primary}
          maskOpacity={0.3}
          edgeColor={theme.colors?.primary}
          animatedLineColor={theme.colors?.accent}
        />
        <Pressable
          onPress={setShowFlashlight}
          style={styles.flashlightContainer}>
          {(showFlashlight && (
            <Icon
              name="flash-off"
              type="ionicon"
              tvParallaxProperties
              hasTVPreferredFocus
              size={widthPercentageToDP(10)}
              color={'rgba(255,255,255,1)'}
            />
          )) || (
            <Icon
              name="flash"
              type="ionicon"
              tvParallaxProperties
              hasTVPreferredFocus
              size={widthPercentageToDP(10)}
              color={'rgba(255,255,255,0.5)'}
            />
          )}
        </Pressable>
      </RNCamera>
    </View>
  );
};

ScanQr.screenName = screens.SCAN_QR;
ScanQr.options = () => {
  return {
    topBar: {
      visible: false,
      drawBehind: true,
      background: {
        color: '#02418b',
      },
    },
  };
};
export default ScanQr;
