import React from 'react';
import { View } from 'react-native';
import styles from './PickingDetail.styles';
import Layout from 'components/Layout';
import { ScrollView } from 'react-native-gesture-handler';
import ItemDetail from 'components/ItemDetail';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import { ListItem, Text } from 'react-native-elements';
import screens from 'screens';
import usePickingProduct from 'modules/Picking/hooks/usePickingProduct';
import Loading from 'components/Loading';
import get from 'lodash.get';

type Props = PickingData & {};
const PickingDetail: ScreenFC<Props> = props => {
  const { data: products = [], loading } = usePickingProduct(props.move_ids);
  return (
    <Layout>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            {props.name}
          </Text>
        </View>

        <View style={styles.detail}>
          <ItemDetail title="Partner" value={props.partner_id} />
          <ItemDetail
            title="Source Location"
            value={props.pick_source_location_id}
          />
          <ItemDetail title="No SO" value={props.pick_origin} />
          <ItemDetail title="Gudang" value={props.picking_type_id} />
          <ItemDetail title="Branch" value={props.company_id} />

          <ItemDetail title="Nama Mandor" value={props.leader_name} />
          <ItemDetail title="Jumlah Buruh" value={props.work_number} />
          <ItemDetail title="Jenis Alat" value={props.tools_name} />
          <ItemDetail title="Jumlah Alat" value={props.tools_number} />
        </View>

        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            Products
          </Text>
        </View>
        <View style={styles.detail}>
          {products.map(product => (
            <ListItem key={product.id} hasTVPreferredFocus tvParallaxProperties>
              <ListItem.Content>
                <ListItem.Title>
                  {get(product, 'product_id.1', '') || ''}
                </ListItem.Title>
                <ListItem.Subtitle>
                  Initial Demand: {get(product, 'product_uom_qty')}
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                  Done: {get(product, 'quantity_done')}
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                  Unit of Measure: {get(product, 'product_uom_id')}
                </ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>
          ))}
        </View>
      </ScrollView>

      <Loading isVisible={loading} />
    </Layout>
  );
};
PickingDetail.screenName = screens.PICKING_DETAIL;
PickingDetail.options = props => {
  return {
    topBar: {
      title: {
        text: props.name,
      },
    },
  };
};

export default PickingDetail;
