import { getPickingList } from 'modules/Picking/services';
import useSWRInfinite from 'swr/infinite';

const getKey = (pageIndex: number, previousPageData: any) => {
  if (previousPageData && !previousPageData.length) {
    return null;
  } // reached the end

  return `/api/get-picking-list?page=${pageIndex}&limit=10&offset=${
    pageIndex * 10
  }`;
};

const usePickingList = () => {
  const { data, size, setSize, error, ...rest } = useSWRInfinite(
    getKey,
    getPickingList,
  );

  const isLoadingInitialData = !data && !error;
  const isLoadingMore =
    isLoadingInitialData ||
    (size > 0 && data && typeof data[size - 1] === 'undefined');

  return {
    data: data || [],
    loading: isLoadingMore || isLoadingInitialData,
    size,
    setSize,
    ...rest,
  };
};
export default usePickingList;
