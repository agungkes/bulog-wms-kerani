// import getStockPickingType from 'services/getStockPickingType';
import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getPickingList = async (props: string) => {
  const pagination = getObjectFromSearch(props);

  const data = await odoo.search_read<PickingData[]>('vit.picking', {
    context: { lang: 'en_US', tz: 'Asia/Jakarta' },
    fields: [
      'state',
      'name',
      'pick_picking_id',
      'partner_id',
      'pick_dest_location_id',
      'pick_source_location_id',
      'date',
      'pick_origin',
      'picking_type_code',
      'move_ids',
      'leader_name',
      'work_number',
      'tools_number',
      'tools_name',
      'company_id',
      'picking_type_id',
      'note',
      'message_follower_ids',
      'message_ids',
      'display_name',
    ],
    sort: 'state ASC',
    limit: Number(pagination.limit),
    offset: Number(pagination.offset),
  });

  return data;
};

export default getPickingList;
