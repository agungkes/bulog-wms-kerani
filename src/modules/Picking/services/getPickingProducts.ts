import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getPickingProducts = async (props: string) => async () => {
  const url = getObjectFromSearch(props);
  const ids = url.ids.split(',').map((e: string) => Number(e));
  const data = await odoo.get<PickingProduct[]>('stock.move', {
    args: [
      ids,
      [
        'name',
        'date_expected',
        'state',
        'picking_type_id',
        'location_id',
        'location_dest_id',
        'scrapped',
        'picking_code',
        'product_type',
        'show_details_visible',
        'show_reserved_availability',
        'show_operations',
        'additional',
        'has_move_lines',
        'is_locked',
        'product_id',
        'is_initial_demand_editable',
        'is_quantity_done_editable',
        'product_uom_qty',
        'reserved_availability',
        'quantity_done',
        'product_uom',
      ],
    ],
    kwargs: {},
  });

  return data || [];
};

export default getPickingProducts;
