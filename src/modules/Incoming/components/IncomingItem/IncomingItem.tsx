import React from 'react';
import { View } from 'react-native';
import styles from './IncomingItem.styles';

import DropShadow from 'react-native-drop-shadow';
import { Divider, ListItem, Text } from 'react-native-elements';

type ItemProps = {
  title: string;
  value: string | number;
};
const Item: React.FC<ItemProps> = ({ title, value }) => (
  <View style={styles.itemContainer}>
    <ListItem.Subtitle style={styles.itemLeftText}>{title}</ListItem.Subtitle>
    <ListItem.Title style={styles.itemRightText}>{value}</ListItem.Title>
  </View>
);

type IncomingItemProps = IncomingData & {
  onPress?: () => void;
};
const IncomingItem: React.FC<IncomingItemProps> = ({ onPress, ...props }) => {
  return (
    <DropShadow style={styles.container}>
      <ListItem hasTVPreferredFocus tvParallaxProperties onPress={onPress}>
        <ListItem.Content>
          <View style={styles.header}>
            <Text style={styles.headerTextSurat}>{props.no_surat_jalan}</Text>
            <Text style={styles.headerTextDate}>{props.partner}</Text>
          </View>
          <Divider style={styles.divider} />

          <Item title="No. PO" value={props.no_po} />
          <Item title="Gudang" value={props.gudang} />
          <Item title="Product" value={props.product} />
          <Item title="Kuantitas" value={props.quantity} />
          <Item title="Tanggal" value={props.created_at} />
        </ListItem.Content>
      </ListItem>
    </DropShadow>
  );
};
export default IncomingItem;
