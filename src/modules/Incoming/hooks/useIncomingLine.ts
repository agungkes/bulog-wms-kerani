import { getIncomingLine } from 'modules/Incoming/services';
import useSWR from 'swr';

const useIncomingLine = (stockPickingMoveLineId?: number | number[]) => {
  const { data } = useSWR(
    '/api/get-incoming-line',
    stockPickingMoveLineId ? getIncomingLine(stockPickingMoveLineId) : null,
  );

  return {
    data,
    loading: !data,
  };
};
export default useIncomingLine;
