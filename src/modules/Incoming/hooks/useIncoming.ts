import { getIncoming } from 'modules/Incoming/services';
import useSWR from 'swr';

const useIncoming = (stockPickingId: number | number[]) => {
  const { data } = useSWR<IncomingDetail>(
    '/api/get-incoming',
    getIncoming(stockPickingId),
  );

  return {
    data,
    loading: !data,
  };
};
export default useIncoming;
