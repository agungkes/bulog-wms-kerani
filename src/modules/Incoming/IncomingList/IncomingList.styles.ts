import { StyleSheet } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  IncomingListWrapper: {
    padding: widthPercentageToDP(4),
  },
});

export default styles;
