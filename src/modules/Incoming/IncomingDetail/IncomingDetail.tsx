import React, { Fragment } from 'react';
import { View } from 'react-native';
import styles from './IncomingDetail.styles';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import Layout from 'components/Layout';
import { ScrollView } from 'react-native-gesture-handler';
import { ListItem, Text } from 'react-native-elements';
import ItemDetail from 'components/ItemDetail';
import useIncomingLine from 'modules/Incoming/hooks/useIncomingLine';
import Loading from 'components/Loading';

import get from 'lodash.get';

type Props = IncomingData & {};
const IncomingDetail: ScreenFC<Props> = props => {
  const { data: products = [], loading } = useIncomingLine(props.move_lines);

  return (
    <Layout>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            {props.name}
          </Text>
        </View>

        <View style={styles.detail}>
          <ItemDetail title="Status" value={props.state} />
          <ItemDetail title="Partner" value={props.partner_id} />
          <ItemDetail title="Source Location" value={props.location_id} />
          <ItemDetail title="Nama Gudang" value={props.picking_type_id} />
          <ItemDetail
            title="Destination Location"
            value={props.location_dest_id}
          />
          <ItemDetail title="Scheduled Date" value={props.scheduled_date} />
          <ItemDetail title="NO PO" value={props.origin} />
          <ItemDetail title="Ref. No Faks" value={props.ref_fax} />
        </View>

        {products.length > 0 && (
          <Fragment>
            <View style={styles.header}>
              <Text h4 style={styles.headerText}>
                Products
              </Text>
            </View>
            <View style={styles.detail}>
              {products.map(product => (
                <ListItem
                  key={product.id}
                  hasTVPreferredFocus
                  tvParallaxProperties>
                  <ListItem.Content>
                    <ListItem.Title>
                      {get(product, 'product_id.1', '') || ''}
                    </ListItem.Title>
                    <ListItem.Subtitle>
                      Quantity: {get(product, 'product_uom_qty')}
                    </ListItem.Subtitle>
                    <ListItem.Subtitle>
                      JT 1: {get(product, 'jt1')}
                    </ListItem.Subtitle>
                    <ListItem.Subtitle>
                      JT 2: {get(product, 'jt2')}
                    </ListItem.Subtitle>
                    <ListItem.Subtitle>
                      JT Bruto: {get(product, 'jtnetto')}
                    </ListItem.Subtitle>
                    <ListItem.Subtitle>
                      Digital/Analog: {get(product, 'digital_analog')}
                    </ListItem.Subtitle>
                    <ListItem.Subtitle>
                      Done: {get(product, 'quantity_done')}
                    </ListItem.Subtitle>
                    <ListItem.Subtitle>
                      Unit of Measure: {get(product, 'product_uom_id')}
                    </ListItem.Subtitle>
                  </ListItem.Content>
                </ListItem>
              ))}
            </View>
          </Fragment>
        )}
      </ScrollView>

      <Loading isVisible={loading} />
    </Layout>
  );
};

IncomingDetail.screenName = screens.INCOMING_DETAIL;
IncomingDetail.options = props => {
  return {
    topBar: {
      title: {
        text: props.name,
      },
    },
  };
};

export default IncomingDetail;
