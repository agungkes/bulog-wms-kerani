import odoo from 'services/odoo.client';

const getIncoming = (stockPickingIds: number | number[]) => async () => {
  // await odoo.connect();

  const data = await odoo.get('stock.picking', {
    args: [stockPickingIds],
    fields: [
      'name',
      'branch_id',
      'location_id',
      'location_dest_id',
      'partner_id',
      'date',
      'scheduled_date',
      'origin',
      'ref_fax',
      'group_id',
      'backorder_id',
      'state',
      'state_inv',
      'invoice_paid',
      'priority',
      'picking_type_id',
      'batch_id',
      'move_lines',
    ],
  });
  console.log(data);

  // return data[0] || {};
  return {};
};

export default getIncoming;
