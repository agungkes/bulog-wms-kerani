import React from 'react';
import Layout from 'components/Layout';
import { Button, ListItem, Text } from 'react-native-elements';
import styles from './SuratJalanDetail.styles';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import { ScrollView } from 'react-native-gesture-handler';
import { View } from 'react-native';

type SuratJalanDetailProps = SuratJalanData;
const SuratJalanDetail: ScreenFC<SuratJalanDetailProps> = props => {
  return (
    <Layout>
      <ScrollView contentContainerStyle={styles.container}>
        <Text h4>{props.no_surat_jalan}</Text>
        <Text>{props.created_at}</Text>

        <View style={styles.detail}>
          <ListItem hasTVPreferredFocus tvParallaxProperties>
            <ListItem.Content>
              <ListItem.Title>No. Surat Jalan</ListItem.Title>
            </ListItem.Content>
            <ListItem.Subtitle>{props.no_surat_jalan}</ListItem.Subtitle>
          </ListItem>
          <ListItem hasTVPreferredFocus tvParallaxProperties>
            <ListItem.Content>
              <ListItem.Title>No. PO</ListItem.Title>
            </ListItem.Content>
            <ListItem.Subtitle>{props.no_po}</ListItem.Subtitle>
          </ListItem>
          <ListItem hasTVPreferredFocus tvParallaxProperties>
            <ListItem.Content>
              <ListItem.Title>Gudang</ListItem.Title>
            </ListItem.Content>
            <ListItem.Subtitle>{props.gudang}</ListItem.Subtitle>
          </ListItem>
          <ListItem hasTVPreferredFocus tvParallaxProperties>
            <ListItem.Content>
              <ListItem.Title>Product</ListItem.Title>
            </ListItem.Content>
            <ListItem.Subtitle>{props.product}</ListItem.Subtitle>
          </ListItem>
          <ListItem hasTVPreferredFocus tvParallaxProperties>
            <ListItem.Content>
              <ListItem.Title>Kuantitas</ListItem.Title>
            </ListItem.Content>
            <ListItem.Subtitle>{props.quantity}</ListItem.Subtitle>
          </ListItem>
        </View>

        <Button title={'Confirm'} />
      </ScrollView>
    </Layout>
  );
};

SuratJalanDetail.screenName = screens.SURAT_JALAN_DETAIL;
SuratJalanDetail.options = props => {
  return {
    topBar: {
      title: {
        text: props.no_surat_jalan,
      },
    },
  };
};
export default SuratJalanDetail;
