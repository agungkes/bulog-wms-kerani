import React, { useRef } from 'react';
import Layout from 'components/Layout';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './SuratJalanList.styles';
import { FlatList } from 'react-native-gesture-handler';

import { useNavigation } from 'react-native-navigation-hooks';
import SuratJalanItem from '../components/SuratJalanItem';

export type SuratJalanData = {
  id: string;
  no_surat_jalan: string;
  no_po: string;
  gudang: string;
  product: string;
  quantity: string;
  created_at: string;
};

const DEMO_DATA: SuratJalanData[] = [];
for (let index = 0; index < 10; index++) {
  DEMO_DATA.push({
    id: `${index}`,
    no_surat_jalan: 'SJ00001',
    no_po: 'PO00199212',
    gudang: 'Rak 1',
    product: 'Beras Medium',
    quantity: '1.00 Pack 50 KG',
    created_at: '11 Desember 2021',
  });
}
const SuratJalanList: ScreenFC = () => {
  const navigation = useNavigation();
  const ref = useRef<FlatList>(null);

  const handleGoToDetail = (item: SuratJalanData) => () => {
    navigation.push(screens.SURAT_JALAN_DETAIL, item);
  };
  const renderItem = ({ item }: { item: SuratJalanData }) => {
    return <SuratJalanItem {...item} onPress={handleGoToDetail(item)} />;
  };
  const keyExtractor = (item: SuratJalanData) => `${item.id}`;
  return (
    <Layout>
      <FlatList
        ref={ref}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        data={DEMO_DATA}
        contentContainerStyle={styles.container}
        initialScrollIndex={0}
        alwaysBounceVertical
      />
    </Layout>
  );
};

SuratJalanList.screenName = screens.SURAT_JALAN_LIST;
SuratJalanList.options = () => {
  return {
    topBar: {
      drawBehind: false,
      visible: true,
      title: {
        text: 'Surat Jalan',
      },
    },
  };
};
export default SuratJalanList;
