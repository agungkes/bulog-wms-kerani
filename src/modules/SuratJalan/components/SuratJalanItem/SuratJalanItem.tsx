import Item from 'components/Item';
import { SuratJalanData } from 'modules/SuratJalan/SuratJalanList/SuratJalanList';
import React from 'react';
import { View } from 'react-native';
import DropShadow from 'react-native-drop-shadow';
import { Divider, ListItem as RNEListItem, Text } from 'react-native-elements';
import styles from './SuratJalanItem.styles';

export type SuratJalanItemProps = SuratJalanData & {
  onPress?: () => void;
};

const SuratJalanItem: React.FC<SuratJalanItemProps> = ({
  no_surat_jalan,
  onPress,
  ...props
}) => {
  return (
    <DropShadow style={styles.container}>
      <RNEListItem hasTVPreferredFocus tvParallaxProperties onPress={onPress}>
        <RNEListItem.Content>
          <View style={styles.header}>
            <Text style={styles.headerTextSurat}>{no_surat_jalan}</Text>
            <Text style={styles.headerTextDate}>{props.created_at}</Text>
          </View>
          <Divider style={styles.divider} />

          <Item title="No. PO" value={props.no_po} />
          <Item title="Gudang" value={props.gudang} />
          <Item title="Product" value={props.product} />
          <Item title="Kuantitas" value={props.quantity} />
        </RNEListItem.Content>
      </RNEListItem>
    </DropShadow>
  );
};

export default SuratJalanItem;
