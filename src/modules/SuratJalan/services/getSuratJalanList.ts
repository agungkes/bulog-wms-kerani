import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

const getSuratJalanList = async (props: string) => {
  const pagination = getObjectFromSearch(props);

  const data = await odoo.search_read<SuratJalanData[]>('vit.qc.inspection', {
    context: { lang: 'en_US', tz: 'Asia/Jakarta' },
    fields: ['name', 'user', 'test', 'qty', 'product_id', 'success', 'state'],
    sort: 'state DESC',
    limit: Number(pagination.limit),
    offset: Number(pagination.offset),
  });

  return data;
};

export default getSuratJalanList;
