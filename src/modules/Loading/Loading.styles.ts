import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(50,50,50,0.25)',
    justifyContent: 'center',
    alignItems: 'center',
    height: heightPercentageToDP('100%'),
    width: widthPercentageToDP('100%'),
    position: 'absolute',
  },

  logoContainer: {
    padding: widthPercentageToDP(2),
    flexDirection: 'row',
    backgroundColor: '#fff',
    alignItems: 'center',
    borderRadius: widthPercentageToDP(1),
  },
  logo: {
    width: widthPercentageToDP(50),
    height: heightPercentageToDP(7),
    marginRight: widthPercentageToDP(2),
  },
});

export default styles;
