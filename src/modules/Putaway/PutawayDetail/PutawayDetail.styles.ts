import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    padding: widthPercentageToDP(4),
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    textAlign: 'center',
  },

  detail: {
    marginVertical: heightPercentageToDP(1),
  },

  approveBtn: {
    marginBottom: heightPercentageToDP(1),
  },
});

export default styles;
