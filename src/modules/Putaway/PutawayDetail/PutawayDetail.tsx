import ItemDetail from 'components/ItemDetail';
import Layout from 'components/Layout';
import Loading from 'components/Loading';
import get from 'lodash.get';
import usePutawayProductList from 'modules/Putaway/hooks/usePutawayProductList';
import React from 'react';
import { View } from 'react-native';
import { ListItem, Text } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './PutawayDetail.styles';

type PutawayDetailProps = PutawayData & {};
const PutawayDetail: ScreenFC<PutawayDetailProps> = props => {
  const { data: products = [], loading } = usePutawayProductList(
    props.move_ids,
  );

  // const handlePressEdit = () => {
  //   toggleEdit();
  // };
  return (
    <Layout>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            {props.name}
          </Text>
        </View>

        <View style={styles.detail}>
          <ItemDetail title="No PO" value={props.put_origin} />
          <ItemDetail title="Partner" value={props.partner_id} />
          <ItemDetail
            title="Source Location"
            value={props.put_source_location_id}
          />
          <ItemDetail
            title="Destination Location"
            value={props.put_dest_location_id}
            isShow={false}
          />

          <ItemDetail title="Nama Mandor" value={props.leader_name} />
          <ItemDetail title="Jumlah Buruh" value={props.work_number} />
          <ItemDetail title="Jenis Alat" value={props.tools_name} />
          <ItemDetail title="Jumlah Alat" value={props.tools_number} />
        </View>

        <View style={styles.header}>
          <Text h4 style={styles.headerText}>
            Products
          </Text>
        </View>
        <View style={styles.detail}>
          {products.map(product => (
            <ListItem key={product.id} hasTVPreferredFocus tvParallaxProperties>
              <ListItem.Content>
                <ListItem.Title>
                  {get(product, 'product_id.1', '') || ''}
                </ListItem.Title>
                <ListItem.Subtitle>
                  Initial Demand: {get(product, 'product_uom_qty')}
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                  Done: {get(product, 'quantity_done')}
                </ListItem.Subtitle>
                <ListItem.Subtitle>
                  Unit of Measure: {get(product, 'product_uom_id')}
                </ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>
          ))}
        </View>
      </ScrollView>

      <Loading isVisible={loading} />
    </Layout>
  );
};

PutawayDetail.screenName = screens.PUTAWAY_DETAIL;
PutawayDetail.options = props => {
  return {
    topBar: {
      title: {
        text: props.name || '',
      },
    },
  };
};
export default PutawayDetail;
