import React from 'react';
import { View } from 'react-native';
import styles from './PutawayItem.styles';

import DropShadow from 'react-native-drop-shadow';
import { Divider, ListItem as RNEListItem, Text } from 'react-native-elements';
import Item from 'components/Item';
import state from 'helpers/state';

export type PutawayItemProps = PutawayData & {
  onPress?: () => void;
};
const PutawayItem: React.FC<PutawayItemProps> = ({ onPress, ...props }) => {
  return (
    <DropShadow style={styles.container}>
      <RNEListItem hasTVPreferredFocus tvParallaxProperties onPress={onPress}>
        <RNEListItem.Content>
          <View style={styles.header}>
            <Text style={styles.headerTextSurat}>{props.name}</Text>
            <Text style={styles.headerTextDate}>{state[props.state]}</Text>
          </View>
          <Divider style={styles.divider} />

          <Item title="Partner" value={props.partner_id} />
          <Item title="Scheduled Date" value={props.date} />
          <Item title="Source Document" value={props.put_origin} />
          <Item
            title="Destination Location"
            value={props.put_dest_location_id}
          />
          <Item title="Source Location" value={props.put_source_location_id} />
        </RNEListItem.Content>
      </RNEListItem>
    </DropShadow>
  );
};

export default PutawayItem;
