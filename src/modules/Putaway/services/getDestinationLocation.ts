import odoo from 'services/odoo.client';

const getDestinationLocation = async () => {
  const data = await odoo.search_read('stock.location', {
    domain: [],
    context: { lang: 'en_US', tz: 'Asia/Jakarta' },
    limit: 10,
  });

  return data || [];
};

export default getDestinationLocation;
