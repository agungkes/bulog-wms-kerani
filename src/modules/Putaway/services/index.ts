import getPutawayList from 'modules/Putaway/services/getPutawayList';
import getPutawayProduct from 'modules/Putaway/services/getPutawayProduct';
import getDestinationLocation from 'modules/Putaway/services/getDestinationLocation';

export { getPutawayList, getPutawayProduct, getDestinationLocation };
