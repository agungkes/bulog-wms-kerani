import getObjectFromSearch from 'helpers/getObjectFromSearch';
import odoo from 'services/odoo.client';

interface PutawayProduct {
  is_quantity_done_editable: boolean;
  show_details_visible: boolean;
  has_move_lines: boolean;
  is_locked: boolean;
  quantity_done: number;
  product_id: [number, string];
  reserved_availability: number;
  additional: boolean;
  state: string;
  product_uom_qty: number;
  location_dest_id: [number, string];
  scrapped: boolean;
  picking_type_id: [number, string];
  is_initial_demand_editable: boolean;
  id: number;
  location_id: [number, string];
  show_operations: boolean;
  show_reserved_availability: boolean;
  product_uom: [number, string];
  picking_code: boolean;
  product_type: string;
  date_expected: string;
  name: string;
}

const getPutawayProduct = async (props: string) => {
  const url = getObjectFromSearch(props);
  const ids = url.ids.split(',').map((e: string) => Number(e));
  const data = await odoo.get<PutawayProduct[]>('stock.move', {
    args: [
      ids,
      [
        'name',
        'date_expected',
        'state',
        'picking_type_id',
        'location_id',
        'location_dest_id',
        'scrapped',
        'picking_code',
        'product_type',
        'show_details_visible',
        'show_reserved_availability',
        'show_operations',
        'additional',
        'has_move_lines',
        'is_locked',
        'product_id',
        'is_initial_demand_editable',
        'is_quantity_done_editable',
        'product_uom_qty',
        'reserved_availability',
        'quantity_done',
        'product_uom',
      ],
    ],
    kwargs: {},
  });

  return data || [];
};

export default getPutawayProduct;
