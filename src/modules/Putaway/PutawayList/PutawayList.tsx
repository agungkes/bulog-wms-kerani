import React from 'react';
import styles from './PutawayList.styles';

import { useNavigation } from 'react-native-navigation-hooks';
import screens from 'screens';
import Layout from 'components/Layout';
import PutawayItem from 'modules/Putaway/components/PutawayItem';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import { FlatList } from 'react-native';
import usePutawayList from 'modules/Putaway/hooks/usePutawayList';
import FlatListEmpty from 'components/FlatListEmpty';
import Loading from 'components/Loading';

const PutawayList: ScreenFC = () => {
  const navigation = useNavigation();

  const { data, loading, setSize, size } = usePutawayList();

  const handleGoToDetail = (item: PutawayData) => () => {
    navigation.push(screens.PUTAWAY_DETAIL, item);
  };
  const renderItem = ({ item }: { item: PutawayData }) => {
    return <PutawayItem {...item} onPress={handleGoToDetail(item)} />;
  };
  const keyExtractor = (item: PutawayData) => `${item.id}`;

  const onEndReached = () => {
    setSize(size + 1);
  };

  return (
    <Layout>
      {!loading && (
        <FlatList
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          data={data.flat(1)}
          contentContainerStyle={styles.PutawayListWrapper}
          alwaysBounceVertical
          scrollEventThrottle={16}
          onEndReached={onEndReached}
          onEndReachedThreshold={0.5}
          ListEmptyComponent={FlatListEmpty}
        />
      )}

      <Loading isVisible={loading} />
    </Layout>
  );
};

PutawayList.screenName = screens.PUTAWAY_LIST;
PutawayList.options = () => ({
  topBar: {
    title: {
      text: 'Putaway',
    },
  },
});
export default PutawayList;
