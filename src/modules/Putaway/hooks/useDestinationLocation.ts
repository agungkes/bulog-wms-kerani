import { getDestinationLocation } from 'modules/Putaway/services';
import useSWR from 'swr';

const useDestinationLocation = () => {
  const { data } = useSWR(
    '/api/get-destination-location',
    getDestinationLocation,
  );

  return {
    data: data || [],
    loading: !data,
  };
};

export default useDestinationLocation;
