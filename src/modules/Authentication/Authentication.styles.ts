import { Dimensions, StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {
    padding: widthPercentageToDP(5),
  },
  backgroundImage: {
    width: screen.width,
    height: screen.height,
    ...StyleSheet.absoluteFillObject,
  },

  logo: {
    width: '100%',
    height: heightPercentageToDP(11),
    marginTop: heightPercentageToDP(17),
  },
  textRight: {
    marginLeft: 'auto',
  },
  text: {
    fontWeight: 'bold',
  },
  textDescription: {
    textAlign: 'center',
    fontSize: widthPercentageToDP(5),
    marginTop: heightPercentageToDP(1.5),
  },
  keyboardAvoiding: {
    flex: 1,
    display: 'flex',
  },
});

export default styles;
