import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import converDpToPixel from '../../../../helpers/convertDpToPixel';

const styles = StyleSheet.create({
  container: {
    padding: widthPercentageToDP(5),
  },

  header: {
    padding: widthPercentageToDP(5),
  },
  headerText: {
    fontSize: widthPercentageToDP(converDpToPixel(20)),
    color: '#000',
  },
  dialogTitle: {
    fontSize: 21,
    color: '#313154',
  },
  dialogText: {
    fontSize: heightPercentageToDP(converDpToPixel(6)),
    color: 'purple',
  },
  dialogImportantText: {
    fontSize: heightPercentageToDP(converDpToPixel(6)),
    color: 'red',
  },

  protokolText: {
    color: '#616161',
    marginTop: 25,
  },
  databaseText: {
    color: '#616161',
  },
  radioContainer: {
    padding: 10,
    marginBottom: 20,
  },

  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  button: {
    width: widthPercentageToDP(20),
  },
});

export default styles;
