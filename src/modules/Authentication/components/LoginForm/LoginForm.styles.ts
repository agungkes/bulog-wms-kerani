import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import converDpToPixel from '../../../../helpers/convertDpToPixel';

const styles = StyleSheet.create({
  dropShadow: {
    shadowColor: '#999999',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.7,
    shadowRadius: 5,
  },
  authContainer: {
    padding: widthPercentageToDP(converDpToPixel(20)),
    borderRadius: widthPercentageToDP(converDpToPixel(6)),
    backgroundColor: 'white',
    marginTop: heightPercentageToDP(converDpToPixel(32)),
  },
  keyboardAvoiding: {
    flex: 1,
    display: 'flex',
  },
});

export default styles;
