import Layout from '@components/Layout';
import IconBox from 'components/IconBox';
import React, { useCallback } from 'react';
import { View } from 'react-native';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';

import styles from './Home.styles';
import screens, { authNavigation } from 'screens';
import {
  useNavigation,
  useNavigationButtonPress,
} from 'react-native-navigation-hooks';
import Icon from 'react-native-vector-icons/Ionicons';
import { storeServer } from 'stores/server.stores';
import { Button, Overlay, Text } from 'react-native-elements';
import useToggle from 'hooks/useToggle';

const NotificationIcon = Icon.getImageSourceSync(
  'notifications-outline',
  24,
  '#fff',
);

const Home: ScreenFC = () => {
  const navigation = useNavigation();
  const [showLogoutConfirmation, toggleLogoutConfirmation] = useToggle();

  const handleGoToNotification = useCallback(() => {
    navigation.push(screens.NOTIFICATION);
  }, [navigation]);
  useNavigationButtonPress(handleGoToNotification, screens.NOTIFICATION);

  const handleGoToSuratJalan = () => {
    navigation.push(screens.SURAT_JALAN_LIST);
  };
  const handleGoToIncoming = () => {
    navigation.push(screens.INCOMING_LIST);
  };
  const handleScanQR = () => {
    navigation.push(screens.SCAN_QR);
  };
  const handleGoToPutaway = () => {
    navigation.push(screens.PUTAWAY_LIST);
  };
  const handleGoToPicking = () => {
    navigation.push(screens.PICKING_LIST);
  };
  const handleLogout = async () => {
    await storeServer.clearStoredDate();
    authNavigation();
  };
  return (
    <Layout>
      <View style={styles.container}>
        <IconBox
          source={require('../../assets/images/scan_code.png')}
          title="Scan QR Code"
          onPress={handleScanQR}
        />
        <IconBox
          source={require('../../assets/images/bill.png')}
          title="Surat Jalan"
          onPress={handleGoToSuratJalan}
        />
        <IconBox
          source={require('../../assets/images/incoming.png')}
          title="Incoming"
          onPress={handleGoToIncoming}
        />
        <IconBox
          source={require('../../assets/images/putaway.png')}
          title="Putaway"
          onPress={handleGoToPutaway}
        />
        <IconBox
          source={require('../../assets/images/picking.png')}
          title="Picking"
          onPress={handleGoToPicking}
        />
        <IconBox
          source={require('../../assets/images/logout.png')}
          title="Logout"
          onPress={toggleLogoutConfirmation}
        />
      </View>

      <Overlay
        onBackdropPress={toggleLogoutConfirmation}
        isVisible={showLogoutConfirmation}>
        <Text style={styles.logout}>Apakah anda yakin?</Text>
        <Button title="Ya" onPress={handleLogout} />
      </Overlay>
    </Layout>
  );
};

Home.screenName = screens.HOME;
Home.options = () => {
  return {
    topBar: {
      visible: true,
      drawBehind: false,
      background: {
        color: '#02418b',
      },
      title: {
        text: 'Bulog WMS Kerani',
      },
      rightButtons: [
        {
          id: screens.NOTIFICATION,
          icon: NotificationIcon,
        },
      ],
    },
  };
};
export default Home;
