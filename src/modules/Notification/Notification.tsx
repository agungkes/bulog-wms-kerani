import Layout from 'components/Layout';
import React from 'react';
import { FlatList } from 'react-native';
import DropShadow from 'react-native-drop-shadow';
import { ListItem } from 'react-native-elements';
import { ScreenFC } from 'react-native-navigation-register-screens/dist/types';
import screens from 'screens';
import styles from './Notification.styles';

const DEMO_DATA = [
  {
    id: 1,
    title: 'Permintaan Penginputan Jumlah Barang',
    created_at: '11 Desember 2021 11:00',
  },
  {
    id: 2,
    title: 'Permintaan Penginputan Jumlah Barang',
    created_at: '12 Desember 2021 12:00',
  },
  {
    id: 3,
    title: 'Permintaan Penginputan Jumlah Barang',
    created_at: '11 Desember 2021 11:00',
  },
];
const Notification: ScreenFC = () => {
  const keyExtractor = (item: typeof DEMO_DATA[0]) => `${item.id}`;
  const renderItem = ({ item }: { item: typeof DEMO_DATA[0] }) => {
    return (
      <DropShadow style={styles.itemContainer}>
        <ListItem
          hasTVPreferredFocus
          tvParallaxProperties
          containerStyle={styles.item}>
          <ListItem.Content>
            <ListItem.Title>{item.title}</ListItem.Title>
            <ListItem.Subtitle>{item.created_at}</ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>
      </DropShadow>
    );
  };
  return (
    <Layout>
      <FlatList
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        data={DEMO_DATA}
        contentContainerStyle={styles.NotificationWrapper}
      />
    </Layout>
  );
};
Notification.screenName = screens.NOTIFICATION;
Notification.options = () => {
  return {
    topBar: {
      title: {
        text: 'Notifications',
      },
    },
  };
};
export default Notification;
